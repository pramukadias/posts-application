
Demo Video URL: https://www.dropbox.com/sh/cuey00qtoczalct/AAALNg5Vlu_iuEUb0DbHFPNoa?dl=0

Application Information

	  Display Name : CSPost

	  Bundle Identifier : com.pramuka.post

	  Version : 1.0

	  Build No : 1

	  Xcode Version :  11.3

      Language :  Swift 5


Application Features

	  1. Application supports iOS 10 and above.

	  2. Application supports both iPhone and iPad (Universal application).

	  3. Application supports both portrait and landscape modes.

	  4. Posts list view was implemented.

	  5. Post details view was implemented.

	  6. User details view was implemented.

	  7. Adding/Deleting post feature was implemented.

	  8. Save the results of the request using a local database so that the app can still be used (after the first successful connection) offline.

      9. Unit Tests were implemented.

