//
//  CSConstant.swift
//  CSPost
//
//  Created by Pramuka Dias on 3/13/21.
//  Copyright © 2021 Pramuka Dias. All rights reserved.
//

import UIKit

import UIKit

struct CSApplicationInfo {
    static let APP_BASE_URL        =  "http://jsonplaceholder.typicode.com/"
}

struct CSMessages {
    static let NO_INTERNET                = "The Internet connection appears to be offline."
    static let ERROR_MESSAGE              = "Something went wrong"
    static let LOADING_TEXT                = "Wait..."
}

struct CSColors {
    static let APP_THEME_COLOR        =  UIColor.init(red: 54.0/255.0, green: 145.0/255.0, blue: 246.0/255.0, alpha: 1.0)
}

struct CSUserDefaults {
    
    static let POST_COMMENT_USER_INFO_SAVED              = "post_comments_user_info_saved"
}
