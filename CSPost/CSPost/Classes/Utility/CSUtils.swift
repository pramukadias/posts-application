//
//  CSUtils.swift
//  CSPost
//
//  Created by Pramuka Dias on 3/13/21.
//  Copyright © 2021 Pramuka Dias. All rights reserved.
//

import UIKit
import SwiftMessages
import Alamofire
import AlamofireImage

class CSUtils: NSObject {

    class func displayAlert(message: String ,themeStyle: Theme?) {
        let aSwiftMessage = MessageView.viewFromNib(layout: .tabView)
        aSwiftMessage.configureTheme(themeStyle!)
        aSwiftMessage.configureContent(title: "", body: message)
        aSwiftMessage.button?.isHidden = true
        var aSwiftMessageConfig = SwiftMessages.defaultConfig
        aSwiftMessageConfig.presentationStyle = .top
        aSwiftMessageConfig.presentationContext = .window(windowLevel: UIWindow.Level.normal)
        aSwiftMessageConfig.duration = .seconds(seconds: 1.5)
        SwiftMessages.show(config: aSwiftMessageConfig, view: aSwiftMessage)
    }
    
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

extension String {
    
    func trim() -> String{
        return self.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
    }
}

extension UITextField {
    
    func isEmpty() -> Bool{
        return (self.text!.trim().count == 0) ? true : false
    }
    
    func configureTextField(text: String){
        self.attributedPlaceholder = NSAttributedString(string: text, attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 12.0, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
        self.layer.cornerRadius = 5.0
        self.layer.borderColor = UIColor.darkGray.cgColor
        self.layer.borderWidth = 1.0
        self.clipsToBounds = true
    }
   
}

extension UIImageView {
    
    func setupImage(imageUrl: String, imageViewSize: CGSize,placeholderImage: String) {
        self.contentMode = .center
        if imageUrl.count != 0 {
            let encodedImage = imageUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
            let imageurl = URL(string: encodedImage!)
            let filter = AspectScaledToFillSizeFilter(size: (imageViewSize))
            self.af_setImage(withURL: imageurl!, placeholderImage: UIImage.init(named: placeholderImage), filter: filter, completion: { (response) in
                if !(response.error != nil) {
                    self.contentMode = .scaleToFill
                }
            })
        }else {
            self.image = UIImage.init(named: placeholderImage)
            self.contentMode = .scaleAspectFit
        }
    }
    
}

extension UserDefaults {
    static func contains(_ key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
}

extension UILabel {
    
    func setUserDetails(user: CSCUser) {
        let attrsTitle = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 18.0),NSAttributedString.Key.foregroundColor: UIColor.black]
        let attrsValue = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 18.0),NSAttributedString.Key.foregroundColor: UIColor.darkGray]
        let attributedString = NSMutableAttributedString(string:"")
        attributedString.append(NSMutableAttributedString(string:"Name: ", attributes:attrsTitle))
        attributedString.append(NSMutableAttributedString(string:" \(user.name ?? "")\n\n", attributes:attrsValue))
        attributedString.append(NSMutableAttributedString(string:"Username: ", attributes:attrsTitle))
        attributedString.append(NSMutableAttributedString(string:" \(user.username ?? "")\n\n", attributes:attrsValue))
        attributedString.append(NSMutableAttributedString(string:"Email: ", attributes:attrsTitle))
        attributedString.append(NSMutableAttributedString(string:" \(user.email ?? "")\n\n", attributes:attrsValue))
        attributedString.append(NSMutableAttributedString(string:"Street: ", attributes:attrsTitle))
        attributedString.append(NSMutableAttributedString(string:" \(user.street ?? "")\n\n", attributes:attrsValue))
        attributedString.append(NSMutableAttributedString(string:"Suite: ", attributes:attrsTitle))
        attributedString.append(NSMutableAttributedString(string:" \(user.suite ?? "")\n\n", attributes:attrsValue))
        attributedString.append(NSMutableAttributedString(string:"City: ", attributes:attrsTitle))
        attributedString.append(NSMutableAttributedString(string:" \(user.city ?? "")\n\n", attributes:attrsValue))
        attributedString.append(NSMutableAttributedString(string:"Zipcode: ", attributes:attrsTitle))
        attributedString.append(NSMutableAttributedString(string:" \(user.zipcode ?? "")\n\n", attributes:attrsValue))
        attributedString.append(NSMutableAttributedString(string:"Phone: ", attributes:attrsTitle))
        attributedString.append(NSMutableAttributedString(string:" \(user.phone ?? "")\n\n", attributes:attrsValue))
        attributedString.append(NSMutableAttributedString(string:"Website: ", attributes:attrsTitle))
        attributedString.append(NSMutableAttributedString(string:" \(user.website ?? "")\n\n", attributes:attrsValue))
        attributedString.append(NSMutableAttributedString(string:"Company Name: ", attributes:attrsTitle))
        attributedString.append(NSMutableAttributedString(string:" \(user.companyName ?? "")\n\n", attributes:attrsValue))
        attributedString.append(NSMutableAttributedString(string:"Company Catch Phrase: ", attributes:attrsTitle))
        attributedString.append(NSMutableAttributedString(string:" \(user.companyCatchPhrase ?? "")\n\n", attributes:attrsValue))
        attributedString.append(NSMutableAttributedString(string:"Company Bs: ", attributes:attrsTitle))
        attributedString.append(NSMutableAttributedString(string:" \(user.companyBs ?? "")\n\n", attributes:attrsValue))
        self.attributedText = attributedString
    }
}
