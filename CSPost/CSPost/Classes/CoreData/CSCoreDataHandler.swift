//
//  CSCoreDataHandler.swift
//  CSPost
//
//  Created by Pramuka Dias on 3/13/21.
//  Copyright © 2021 Pramuka Dias. All rights reserved.
//

import UIKit
import CoreData

struct CSCoreDataHandler {
    
    static let viewContext: NSManagedObjectContext = {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            fatalError()
        }
        return appDelegate.persistentContainer.viewContext
    }()
    
    static func saveContext() -> Bool {
        do {
            try viewContext.save()
        } catch { return false }
        return true
    }
}

// MARK: Insertion
extension CSCoreDataHandler{
    
    static func savePost(aPost: CSRPost) -> Bool {
        let newPost = NSEntityDescription.insertNewObject(forEntityName: "CSCPost", into: viewContext) as? CSCPost
        newPost?.userId = Int64(aPost.userId ?? 0)
        newPost?.postId =  Int64(aPost.postId ?? 0)
        newPost?.postTitle =  aPost.postTitle ?? ""
        newPost?.postBody =  aPost.postBody ?? ""
        return self.saveContext()
    }
    
    static func saveComments(aComment: CSRComments) -> Bool {
        let newComment = NSEntityDescription.insertNewObject(forEntityName: "CSCComments", into: viewContext) as? CSCComments
        newComment?.commentId = Int64(aComment.commentId ?? 0)
        newComment?.postId =  Int64(aComment.postId ?? 0)
        newComment?.username =  aComment.username ?? ""
        newComment?.email =  aComment.email ?? ""
        newComment?.commentBody =  aComment.commentBody ?? ""
        return self.saveContext()
    }
    
    static func saveUsers(aUser: CSRUser) -> Bool {
        let newUser = NSEntityDescription.insertNewObject(forEntityName: "CSCUser", into: viewContext) as? CSCUser
        newUser?.userId = Int64(aUser.userId ?? 0)
        newUser?.name =  aUser.name ?? ""
        newUser?.username =  aUser.username ?? ""
        newUser?.email =  aUser.email ?? ""
        newUser?.street =  aUser.street ?? ""
        newUser?.suite =  aUser.suite ?? ""
        newUser?.city =  aUser.city ?? ""
        newUser?.zipcode =  aUser.zipcode ?? ""
        newUser?.latitude =  aUser.latitude ?? ""
        newUser?.longitude =  aUser.longitude ?? ""
        newUser?.phone =  aUser.phone ?? ""
        newUser?.website =  aUser.website ?? ""
        newUser?.companyName =  aUser.companyName ?? ""
        newUser?.companyCatchPhrase =  aUser.companyCatchPhrase ?? ""
        newUser?.companyBs =  aUser.companyBs ?? ""
        return self.saveContext()
    }
}

// MARK: Fetching
extension CSCoreDataHandler{
    
    static func getPosts() -> [CSCPost] {
        do {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "CSCPost")
            let result = try viewContext.fetch(request)
            let user = result as! [CSCPost]
            return user
        } catch {
            print("Could not fetch \(error.localizedDescription)")
            return [CSCPost]()
        }
    }
    
    static func getComments(postId: String) -> [CSCComments] {
        do {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "CSCComments")
            let predicate = NSPredicate(format: "postId == %@", postId)
            request.predicate = predicate
            let result = try viewContext.fetch(request)
            let user = result as! [CSCComments]
            return user
        } catch {
            print("Could not fetch \(error.localizedDescription)")
            return [CSCComments]()
        }
    }
    
    static func getUser(userId: String) -> CSCUser {
        do {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "CSCUser")
            let predicate = NSPredicate(format: "userId == %@", userId)
            request.predicate = predicate
            let result = try viewContext.fetch(request)
            if !result.isEmpty{
                let user = result.first ?? CSCUser()
                return user as! CSCUser
            }else{
                let newUser = NSEntityDescription.insertNewObject(forEntityName: "CSCUser", into: viewContext) as? CSCUser
                 newUser?.userId = Int64(-1)
                return newUser ?? CSCUser()
            }
        } catch {
            print("Could not fetch \(error.localizedDescription)")
            return CSCUser()
        }
    }
    
}

// MARK: Deleting
extension CSCoreDataHandler{
    
    static func deletePost(aPost: CSCPost) -> Bool {
         var isDeleted = true
         viewContext.delete(aPost)
         do {
             try viewContext.save()
         } catch {
             isDeleted = false
         }
         return isDeleted
    }
    
}

// MARK: Reset Database
extension CSCoreDataHandler{
    
    static func clearDatabase(entityName: String) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
            do {
                try viewContext.execute(batchDeleteRequest)
            } catch {
                print("Detele all data in \(entityName) error :", error)
            }
    }
}

// MARK: Save API response in CoreData
extension CSCoreDataHandler{
    
    static func savePostResponse(postArray : [CSRPost]) -> Bool{
        CSCoreDataHandler.clearDatabase(entityName: "CSCPost")
        for aPost in postArray{
            guard CSCoreDataHandler.savePost(aPost: aPost) else {
                return false
            }
        }
        return true
    }
    
    static func saveCommentResponse(commentArray : [CSRComments]) -> Bool{
        CSCoreDataHandler.clearDatabase(entityName: "CSCComments")
        for aComment in commentArray{
            guard CSCoreDataHandler.saveComments(aComment: aComment) else {
                return false
            }
        }
        return true
    }
    
    static func saveUsersResponse(userArray : [CSRUser]) -> Bool{
        CSCoreDataHandler.clearDatabase(entityName: "CSCUser")
        for aUser in userArray{
            guard CSCoreDataHandler.saveUsers(aUser: aUser) else {
                return false
            }
        }
        return true
    }
}
