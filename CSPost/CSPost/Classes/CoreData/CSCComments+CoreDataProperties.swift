//
//  CSCComments+CoreDataProperties.swift
//  
//
//  Created by Pramuka Dias on 3/15/21.
//
//

import Foundation
import CoreData


extension CSCComments {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CSCComments> {
        return NSFetchRequest<CSCComments>(entityName: "CSCComments")
    }

    @NSManaged public var commentBody: String?
    @NSManaged public var commentId: Int64
    @NSManaged public var email: String?
    @NSManaged public var postId: Int64
    @NSManaged public var username: String?

}
