//
//  CSCPost+CoreDataProperties.swift
//  
//
//  Created by Pramuka Dias on 3/15/21.
//
//

import Foundation
import CoreData


extension CSCPost {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CSCPost> {
        return NSFetchRequest<CSCPost>(entityName: "CSCPost")
    }

    @NSManaged public var postBody: String?
    @NSManaged public var postId: Int64
    @NSManaged public var postTitle: String?
    @NSManaged public var userId: Int64

}
