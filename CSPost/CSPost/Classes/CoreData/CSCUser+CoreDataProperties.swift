//
//  CSCUser+CoreDataProperties.swift
//  
//
//  Created by Pramuka Dias on 3/15/21.
//
//

import Foundation
import CoreData


extension CSCUser {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CSCUser> {
        return NSFetchRequest<CSCUser>(entityName: "CSCUser")
    }

    @NSManaged public var city: String?
    @NSManaged public var companyBs: String?
    @NSManaged public var companyCatchPhrase: String?
    @NSManaged public var companyName: String?
    @NSManaged public var email: String?
    @NSManaged public var latitude: String?
    @NSManaged public var longitude: String?
    @NSManaged public var name: String?
    @NSManaged public var phone: String?
    @NSManaged public var street: String?
    @NSManaged public var suite: String?
    @NSManaged public var userId: Int64
    @NSManaged public var username: String?
    @NSManaged public var website: String?
    @NSManaged public var zipcode: String?

}
