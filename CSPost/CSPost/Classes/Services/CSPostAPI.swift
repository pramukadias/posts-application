//
//  CSPostAPI.swift
//  CSPost
//
//  Created by Pramuka Dias on 3/13/21.
//  Copyright © 2021 Pramuka Dias. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class CSPostAPI: NSObject {
    
    class func getPosts(onSuccess: @escaping (_ postArray : [CSRPost]) -> Void, onError: @escaping (String) -> Void) {
        let url = CSApplicationInfo.APP_BASE_URL + "posts"
        Alamofire.request(url).responseArray { (response: DataResponse<[CSRPost]>) in
            let statusCode = response.response?.statusCode
            guard statusCode == 200 else {
                onError(CSMessages.ERROR_MESSAGE)
                return
            }
            onSuccess(response.result.value ?? [CSRPost]())
        }
    }
    
    class func getComments(onSuccess: @escaping (_ commentArray : [CSRComments]) -> Void, onError: @escaping (String) -> Void) {
        let url = CSApplicationInfo.APP_BASE_URL + "comments"
        Alamofire.request(url).responseArray { (response: DataResponse<[CSRComments]>) in
            let statusCode = response.response?.statusCode
            guard statusCode == 200 else {
                onError(CSMessages.ERROR_MESSAGE)
                return
            }
            onSuccess(response.result.value ?? [CSRComments]())
        }
    }
    
    class func getUsers(onSuccess: @escaping (_ userArray : [CSRUser]) -> Void, onError: @escaping (String) -> Void) {
        let url = CSApplicationInfo.APP_BASE_URL + "users"
        Alamofire.request(url).responseArray { (response: DataResponse<[CSRUser]>) in
            let statusCode = response.response?.statusCode
            guard statusCode == 200 else {
                onError(CSMessages.ERROR_MESSAGE)
                return
            }
            onSuccess(response.result.value ?? [CSRUser]())
        }
    }
    
    class func savePost(title: String, body: String, onSuccess: @escaping (_ aPost : CSRPost) -> Void, onError: @escaping (String) -> Void) {
        let url = CSApplicationInfo.APP_BASE_URL + "posts"
        let parameters: [String: AnyHashable] = [
            "title" : title,
            "body" : body,
            "userId" : 1,
        ]
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default)
        .responseJSON { response in
            print(response.result.isSuccess)
            switch response.result {
              case .success(let response):
                let postInfo = response as! [String: AnyHashable]
                let aPost = CSRPost()
                aPost.postId = postInfo["id"] as? Int
                aPost.userId = postInfo["userId"] as? Int
                aPost.postTitle = postInfo["title"] as? String
                aPost.postBody = postInfo["body"] as? String
                onSuccess(aPost)
              case .failure( _):
                  onError(CSMessages.ERROR_MESSAGE)
                  break
              }
        }
    }
}
