//
//  CSRPost.swift
//  CSPost
//
//  Created by Pramuka Dias on 3/13/21.
//  Copyright © 2021 Pramuka Dias. All rights reserved.
//

import UIKit
import ObjectMapper

class CSRPost: Mappable {
    
    var userId: Int?
    var postId: Int?
    var postTitle: String?
    var postBody: String?

    init(){}
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        userId <- map["userId"]
        postId <- map["id"]
        postTitle <- map["title"]
        postBody <- map["body"]
    }
}
