//
//  CSRComments.swift
//  CSPost
//
//  Created by Pramuka Dias on 3/13/21.
//  Copyright © 2021 Pramuka Dias. All rights reserved.
//

import UIKit
import ObjectMapper

class CSRComments: Mappable {
    
    var commentId: Int?
    var postId: Int?
    var username: String?
    var email: String?
    var commentBody: String?

    init(){}
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        commentId <- map["id"]
        postId <- map["postId"]
        username <- map["name"]
        email <- map["email"]
        commentBody <- map["body"]
    }
}
