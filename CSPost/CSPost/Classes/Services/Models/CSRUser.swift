//
//  CSRUser.swift
//  CSPost
//
//  Created by Pramuka Dias on 3/13/21.
//  Copyright © 2021 Pramuka Dias. All rights reserved.
//

import UIKit
import ObjectMapper

class CSRUser: Mappable {
    
    var userId: Int?
    var name: String?
    var username: String?
    var email: String?
    var street: String?
    var suite: String?
    var city: String?
    var zipcode: String?
    var latitude: String?
    var longitude: String?
    var phone: String?
    var website: String?
    var companyName: String?
    var companyCatchPhrase: String?
    var companyBs: String?

    init(){}
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        userId <- map["id"]
        name <- map["name"]
        username <- map["username"]
        email <- map["email"]
        street <- map["address.street"]
        suite <- map["address.suite"]
        city <- map["address.city"]
        zipcode <- map["address.zipcode"]
        latitude <- map["address.geo.lat"]
        longitude <- map["address.geo.lng"]
        phone <- map["phone"]
        website <- map["website"]
        companyName <- map["company.name"]
        companyCatchPhrase <- map["company.catchPhrase"]
        companyBs <- map["company.bs"]
    }
}

