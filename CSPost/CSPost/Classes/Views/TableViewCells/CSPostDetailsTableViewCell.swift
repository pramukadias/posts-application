//
//  CSPostDetailsTableViewCell.swift
//  CSPost
//
//  Created by Pramuka Dias on 3/13/21.
//  Copyright © 2021 Pramuka Dias. All rights reserved.
//

import UIKit

class CSPostDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var comentsLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var userButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        avatarImageView.layer.cornerRadius = 30.0
        avatarImageView.clipsToBounds = true
    }
    
    func setPostDeatils(aPost: CSCPost, user: CSCUser?, commentCount: Int){
        titleLabel.text = aPost.postTitle
        descriptionLabel.text = aPost.postBody
        userNameLabel.text =  user?.name ?? ""
        userButton.isHidden = (user?.userId == -1) ? true : false
        comentsLabel.text = (commentCount == 0) ?  "No comment" : "Comments \(commentCount)"
        let imageUrl = "https://api.adorable.io/avatars/250/\(user?.email ?? "")"
        self.avatarImageView.setupImage(imageUrl: imageUrl, imageViewSize: self.avatarImageView.frame.size, placeholderImage: "avatar_man_s")
    }
}
