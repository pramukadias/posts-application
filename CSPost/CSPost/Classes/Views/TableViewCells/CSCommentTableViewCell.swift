//
//  CSCommentTableViewCell.swift
//  CSPost
//
//  Created by Pramuka Dias on 3/13/21.
//  Copyright © 2021 Pramuka Dias. All rights reserved.
//

import UIKit

class CSCommentTableViewCell: UITableViewCell {

    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var comentsLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        avatarImageView.layer.cornerRadius = 30.0
        avatarImageView.clipsToBounds = true
    }
    
    func setCommentDeatils(aComment: CSCComments){
        emailLabel.text = aComment.email
        userNameLabel.text = aComment.username
        comentsLabel.text = aComment.commentBody
        let imageUrl = "https://api.adorable.io/avatars/250/\(aComment.email ?? "")"
        self.avatarImageView.setupImage(imageUrl: imageUrl, imageViewSize: self.avatarImageView.frame.size, placeholderImage: "avatar_man_s")
    }

}
