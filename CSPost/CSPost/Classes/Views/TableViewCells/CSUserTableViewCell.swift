//
//  CSUserTableViewCell.swift
//  CSPost
//
//  Created by Pramuka Dias on 3/14/21.
//  Copyright © 2021 Pramuka Dias. All rights reserved.
//

import UIKit
import MapKit

class CSUserTableViewCell: UITableViewCell {
    
    @IBOutlet weak var userInfoLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var mapView: MKMapView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUserDetails(user: CSCUser){
        userInfoLabel.setUserDetails(user: user)
        setUserLocation(user: user)
    }
    
    func setUserLocation(user: CSCUser) {
        let lat = Double(user.latitude ?? "0") ?? 0
        let long = Double(user.longitude ?? "0") ?? 0
        let location = CLLocationCoordinate2D(latitude: lat, longitude:long)
        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: location, span: span)
        mapView.setRegion(region, animated: true)
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = "\(user.street ?? ""), \(user.city ?? "")"
        mapView.addAnnotation(annotation)
    }
}
