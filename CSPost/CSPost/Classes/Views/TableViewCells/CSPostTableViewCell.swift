//
//  CSPostTableViewCell.swift
//  CSPost
//
//  Created by Pramuka Dias on 3/13/21.
//  Copyright © 2021 Pramuka Dias. All rights reserved.
//

import UIKit

class CSPostTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setPostDetails(aPost: CSCPost){
        titleLabel.text = aPost.postTitle
        descriptionLabel.text = aPost.postBody
    }
}
