//
//  CSPostValidation.swift
//  CSPost
//
//  Created by Pramuka Dias on 3/14/21.
//  Copyright © 2021 Pramuka Dias. All rights reserved.
//

import UIKit

struct CSPostValidation {

    func validateTitle(_ title: String?) -> (Bool, String){
        guard !(title?.isEmpty ?? false) else {
            return (false, "Please enter post title")
        }
         return (true, "Post title validation success")
    }
    
    func validateDescription(_ description: String?) -> (Bool, String){
        guard !(description?.isEmpty ?? false) else {
            return (false, "Please enter post description")
        }
        return (true, "Post description validation success")
    }
}
