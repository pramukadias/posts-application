//
//  CSAddPostViewController.swift
//  CSPost
//
//  Created by Pramuka Dias on 3/14/21.
//  Copyright © 2021 Pramuka Dias. All rights reserved.
//

import UIKit

enum PostViewType {
    case addPost
    case displayUserDetails
}

protocol CSAddPostViewDelegate: class {
    func reloadPostData()
}

class CSAddPostViewController: CSBaseViewController {

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var bodyTextField: UITextField!
    @IBOutlet weak var userTableView: UITableView!
    @IBOutlet weak var addButton: UIButton!

    weak var delegate: CSAddPostViewDelegate?

    var viewType: PostViewType = .addPost
    var user : CSCUser?
    private let postValidation = CSPostValidation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard viewType == .addPost else {
            configureUserView()
            return
        }
        configurePostAddView()
    }
    
}

// MARK: Add Post
extension CSAddPostViewController{
    
    private func configurePostAddView(){
        addButton.layer.cornerRadius = 5.0
        userTableView.isHidden = true
        self.title = "Add Post"
        titleTextField.configureTextField(text: "Title")
        bodyTextField.configureTextField(text: "Description")
    }
    
    @IBAction func addPostButtonAction(_ sender: Any) {
        let (titleValidationStatus, titleMessage) =  postValidation.validateTitle(titleTextField.text?.trim())
        let (descriptionValidationStatus, descriptionMessage) = postValidation.validateDescription(bodyTextField.text?.trim())
        guard titleValidationStatus else {
            CSUtils.displayAlert(message: titleMessage, themeStyle: .error)
            return
        }
        guard descriptionValidationStatus else {
            CSUtils.displayAlert(message: descriptionMessage, themeStyle: .error)
            return
        }
        if !CSUtils.isConnectedToInternet(){return }
        savePost()
    }
    
}

// MARK: API
extension CSAddPostViewController{
    
    private func savePost(){
        if !CSUtils.isConnectedToInternet(){return }
        self.startActivityAnimating(message: CSMessages.LOADING_TEXT)
        CSPostAPI.savePost(title: self.titleTextField.text?.trim() ?? "", body: self.bodyTextField.text?.trim() ?? "", onSuccess: {(_ aPost : CSRPost) -> Void in
            self.stopActicityAnimating()
            guard CSCoreDataHandler.savePost(aPost: aPost) else {
                return
            }
            CSUtils.displayAlert(message: "You have successfully created a post", themeStyle: .success)
            self.navigationController?.popViewController(animated: true)
            self.delegate?.reloadPostData()
        }, onError: {(_ message) -> Void in
            self.stopActicityAnimating()
            CSUtils.displayAlert(message: message, themeStyle: .error)
        })
    }
}

// MARK: User Info
extension CSAddPostViewController{
    
    private func configureUserView(){
        self.title = "User Details"
        self.userTableView.register(UINib(nibName: "CSUserTableViewCell", bundle: nil), forCellReuseIdentifier: "CSUserTableViewCell")
        self.userTableView.dataSource = self
        self.userTableView.delegate = self
    }
}

// MARK: UITableViewDataSource, UITableViewDelegate
extension CSAddPostViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CSUserTableViewCell") as! CSUserTableViewCell
        cell.setUserDetails(user: user ?? CSCUser())
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension;
    }

}
