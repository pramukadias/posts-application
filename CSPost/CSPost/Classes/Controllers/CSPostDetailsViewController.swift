//
//  CSPostDetailsViewController.swift
//  CSPost
//
//  Created by Pramuka Dias on 3/13/21.
//  Copyright © 2021 Pramuka Dias. All rights reserved.
//

import UIKit
import CoreData

class CSPostDetailsViewController: CSBaseViewController {

    @IBOutlet weak var postDetailsTableView: UITableView!
    var didSelectPost : CSCPost?
    private var user :  CSCUser?
    private var commentsArray = [CSCComments]()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }

}

extension CSPostDetailsViewController{
    
    private func configureView(){
        commentsArray = CSCoreDataHandler.getComments(postId: "\(didSelectPost?.postId ?? 0)")
        user = CSCoreDataHandler.getUser(userId: "\(didSelectPost?.userId ?? 0)")
        self.title = "Posts"
        configureTableView()
    }
    
    private func configureTableView(){
        self.postDetailsTableView.register(UINib(nibName: "CSPostTableViewCell", bundle: nil), forCellReuseIdentifier: "CSPostTableViewCell")
        self.postDetailsTableView.dataSource = self
        self.postDetailsTableView.delegate = self
    }
    
    @objc func navigateToUserDetailsView(){
        let addPostView = self.storyboard?.instantiateViewController(withIdentifier: "CSAddPostViewController") as! CSAddPostViewController
        addPostView.viewType = .displayUserDetails
        addPostView.user = self.user
        self.navigationController?.pushViewController(addPostView, animated: true)
    }
}

// MARK: UITableViewDataSource, UITableViewDelegate
extension CSPostDetailsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard section != 0 else {
            return 1
        }
        return commentsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.section != 0 else {
            return configurePostDetailsCell(tableView: tableView, indexPath: indexPath)
        }
        return configureCommentCell(tableView: tableView, indexPath: indexPath)
    }
    
    private func configurePostDetailsCell(tableView: UITableView, indexPath: IndexPath) -> CSPostDetailsTableViewCell{
        self.postDetailsTableView.register(UINib(nibName: "CSPostDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "CSPostDetailsTableViewCell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "CSPostDetailsTableViewCell") as! CSPostDetailsTableViewCell
        cell.setPostDeatils(aPost: didSelectPost ?? CSCPost(), user: user ?? CSCUser(), commentCount: commentsArray.count)
        cell.userButton.addTarget(self, action: #selector(navigateToUserDetailsView), for: .touchUpInside)
        return cell
    }
    
    private func configureCommentCell(tableView: UITableView, indexPath: IndexPath) -> CSCommentTableViewCell{
        self.postDetailsTableView.register(UINib(nibName: "CSCommentTableViewCell", bundle: nil), forCellReuseIdentifier: "CSCommentTableViewCell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "CSCommentTableViewCell") as! CSCommentTableViewCell
        cell.setCommentDeatils(aComment: commentsArray[indexPath.row])
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension;
    }
    
}
