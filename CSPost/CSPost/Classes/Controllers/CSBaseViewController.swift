//
//  CSBaseViewController.swift
//  CSPost
//
//  Created by Pramuka Dias on 3/13/21.
//  Copyright © 2021 Pramuka Dias. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class CSBaseViewController: UIViewController, NVActivityIndicatorViewable {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.delegate = self
    }
    
}

// MARK: Activity indicator
extension CSBaseViewController{
    
    func startActivityAnimating(message: String) {
        if self.isAnimating{
            stopActicityAnimating()
        }
        let size = CGSize(width: 40, height: 40)
        NVActivityIndicatorView.DEFAULT_COLOR = CSColors.APP_THEME_COLOR
        NVActivityIndicatorView.DEFAULT_TEXT_COLOR = CSColors.APP_THEME_COLOR
        startAnimating(size, message: message, type: NVActivityIndicatorType.orbit)
    }
    
    func stopActicityAnimating() {
        self.stopAnimating()
    }
}

// MARK: Customize navigation bar
extension CSBaseViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool){
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = CSColors.APP_THEME_COLOR
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        UINavigationBar.appearance().isTranslucent = false
    }
    
}
