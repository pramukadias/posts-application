//
//  CSPostsViewController.swift
//  CSPost
//
//  Created by Pramuka Dias on 3/13/21.
//  Copyright © 2021 Pramuka Dias. All rights reserved.
//

import UIKit

class CSPostsViewController: CSBaseViewController {

    @IBOutlet weak var postTableView: UITableView!

    private var isDeleted = false
    private var postsArray = [CSCPost]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }

}

extension CSPostsViewController: CSAddPostViewDelegate{
    
    private func configureView(){
        let addBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(navigateToAddPostView))
        self.navigationItem.rightBarButtonItem  = addBarButtonItem
        self.title = "Posts"
        configureTableView()
        if !UserDefaults.contains(CSUserDefaults.POST_COMMENT_USER_INFO_SAVED) || !UserDefaults.standard.bool(forKey: CSUserDefaults.POST_COMMENT_USER_INFO_SAVED) {
            getPosts()
        }else{
            setupTableDataSource()
        }
    }
    
    private func configureTableView(){
        self.postTableView.register(UINib(nibName: "CSPostTableViewCell", bundle: nil), forCellReuseIdentifier: "CSPostTableViewCell")
        self.postTableView.dataSource = self
        self.postTableView.delegate = self
    }
    
    private func setupTableDataSource(){
        postsArray = CSCoreDataHandler.getPosts()
        addTrashBarButton()
        postTableView.reloadData()
        self.postTableView.isHidden = (self.postsArray.count != 0) ?  false : true
    }
    
    func reloadPostData(){
        setupTableDataSource()
    }
    
    @objc func navigateToAddPostView(){
        let addPostView = self.storyboard?.instantiateViewController(withIdentifier: "CSAddPostViewController") as! CSAddPostViewController
        addPostView.viewType = .addPost
        addPostView.delegate = self
        self.navigationController?.pushViewController(addPostView, animated: true)
    }
}

// MARK: Post Deleting
extension CSPostsViewController{
    
    private func addTrashBarButton(){
        if self.postsArray.count != 0 {
            let trashButton = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(self.trashButtonAction))
            self.navigationItem.leftBarButtonItem = trashButton
        }
    }
    
    @objc private func trashButtonAction(){
        self.isDeleted = !self.isDeleted
        self.postTableView.isEditing = self.isDeleted
        let item = (self.isDeleted) ? UIBarButtonItem.SystemItem.cancel : UIBarButtonItem.SystemItem.trash 
        let barButton = UIBarButtonItem(barButtonSystemItem: item, target: self, action: #selector(self.trashButtonAction))
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    private func deletePost(indexPath: IndexPath){
        let aPost = self.postsArray[indexPath.row]
        if CSCoreDataHandler.deletePost(aPost: aPost){
             self.postsArray.remove(at: indexPath.row)
             self.postTableView.deleteRows(at: [indexPath], with: .bottom)
             if self.postsArray.count == 0 {
                 self.navigationItem.leftBarButtonItem = nil
                 isDeleted = false
                 self.postTableView.isEditing = false
                 self.postTableView.isHidden = (self.postsArray.count != 0) ?  false : true
             }
         }else {
             CSUtils.displayAlert(message: CSMessages.ERROR_MESSAGE, themeStyle: .error)
         }
    }
}

// MARK: API
extension CSPostsViewController{
    
    private func getPosts(){
        if !CSUtils.isConnectedToInternet(){ return }
        self.startActivityAnimating(message: CSMessages.LOADING_TEXT)
        CSPostAPI.getPosts(onSuccess: {( postArray : [CSRPost]) -> Void in
            let status = CSCoreDataHandler.savePostResponse(postArray: postArray)
            self.setupTableDataSource()
            self.stopActicityAnimating()
            self.getUsers(postSavedStaus: status)
        }, onError: {(_ message) -> Void in
            self.stopActicityAnimating()
            CSUtils.displayAlert(message: message, themeStyle: .error)
        })
    }
    
    private func getComments(postSavedStaus: Bool, userSavedStaus: Bool){
        if !CSUtils.isConnectedToInternet(){self.stopActicityAnimating();return }
        CSPostAPI.getComments(onSuccess: {( commentArray : [CSRComments]) -> Void in
            let status = CSCoreDataHandler.saveCommentResponse(commentArray: commentArray)
            if status && postSavedStaus && userSavedStaus{
                 UserDefaults.standard.set(true, forKey: CSUserDefaults.POST_COMMENT_USER_INFO_SAVED)
             }
        }, onError: {(_ message) -> Void in})
    }

    private func getUsers(postSavedStaus: Bool){
        if !CSUtils.isConnectedToInternet(){self.stopActicityAnimating();return }
        CSPostAPI.getUsers(onSuccess: {( userArray : [CSRUser]) -> Void in
            let status = CSCoreDataHandler.saveUsersResponse(userArray: userArray)
            self.getComments(postSavedStaus: postSavedStaus, userSavedStaus: status)
        }, onError: {(_ message) -> Void in})
    }

}

// MARK: UITableViewDataSource, UITableViewDelegate
extension CSPostsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CSPostTableViewCell") as! CSPostTableViewCell
        cell.setPostDetails(aPost: postsArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let postDetailsView = self.storyboard?.instantiateViewController(withIdentifier: "CSPostDetailsViewController") as! CSPostDetailsViewController
        postDetailsView.didSelectPost = postsArray[indexPath.row]
        self.navigationController?.pushViewController(postDetailsView, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension;
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
           deletePost(indexPath: indexPath)
        }
    }
}
