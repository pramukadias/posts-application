//
//  CSSavePostCoreDataTest.swift
//  CSPostTests
//
//  Created by Pramuka Dias on 3/14/21.
//  Copyright © 2021 Pramuka Dias. All rights reserved.
//

@testable import CSPost
import XCTest

class CSSavePostCoreDataTest: XCTestCase {

      override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testSavePost(){
        let aPost = CSRPost()
        aPost.postId = 1021
        aPost.userId = 1
        aPost.postTitle = "Test Post Title"
        aPost.postBody = "Test Post Description"
        XCTAssertTrue(CSCoreDataHandler.savePost(aPost: aPost))
    }

}
