//
//  CSUserValidationTest.swift
//  CSPostTests
//
//  Created by Pramuka Dias on 3/14/21.
//  Copyright © 2021 Pramuka Dias. All rights reserved.
//

@testable import CSPost
import XCTest

class CSUserValidationTest: XCTestCase {

    var validation: CSPostValidation!
    
    override func setUp() {
        super.setUp()
        validation = CSPostValidation()
    }
    
    override func tearDown() {
        validation = nil
        super.tearDown()
    }
    
    func testTitleIsValidTest(){
        let (titleValidationStatus, _) = validation.validateTitle("Test Title")
        XCTAssertTrue(titleValidationStatus)
    }
    
    func testTitleIsInvalidTest(){
        let (titleValidationStatus, _) = validation.validateTitle("")
        XCTAssertFalse(titleValidationStatus)
    }
    
    func testDescriptionIsValidTest(){
        let (descriptionValidationStatus, _) = validation.validateDescription("Test Description")
        XCTAssertTrue(descriptionValidationStatus)
    }
    
    func testDescriptionIsInvalidTest(){
        let (descriptionValidationStatus, _) = validation.validateDescription("")
        XCTAssertFalse(descriptionValidationStatus)
    }
}
