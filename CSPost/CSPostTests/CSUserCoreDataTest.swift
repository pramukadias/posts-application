//
//  CSUserCoreDataTest.swift
//  CSPostTests
//
//  Created by Pramuka Dias on 3/14/21.
//  Copyright © 2021 Pramuka Dias. All rights reserved.
//

@testable import CSPost
import XCTest

class CSUserCoreDataTest: XCTestCase {
    
    let aPost = CSRPost()

     override func setUp() {
        super.setUp()
        aPost.postId = 2
        aPost.userId = 2
        aPost.postTitle = "Dummy Title"
        aPost.postBody = "Dummy Body"
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testGetValidUserFromPost(){
        var isValidUser = false
        let anUser = CSCoreDataHandler.getUser(userId: "\(aPost.userId ?? 0)")
        if anUser.userId == aPost.userId ?? 0{
            isValidUser = true
        }
        XCTAssertTrue(isValidUser)
    }
    
    func testGetInvalidUserFromPost(){
        var isInvalidUser = true
        let validUserId = 3
        let anUser = CSCoreDataHandler.getUser(userId: "\(aPost.userId ?? 0)")
        if anUser.userId == validUserId{
            isInvalidUser = false
        }
        XCTAssertTrue(isInvalidUser)
    }
    
    func testHasUserFromPost(){
        var existUser = false
        aPost.userId = 3
        let anUser = CSCoreDataHandler.getUser(userId: "\(aPost.userId ?? 0)")
        if let userId = anUser.userId as Int64? {
            print(userId)
            existUser = true
        }
        XCTAssertTrue(existUser)
    }
    
    func testNoUserFromPost(){
        var noUser = false
        aPost.userId = 3363
        let anUser = CSCoreDataHandler.getUser(userId: "\(aPost.userId ?? 0)")
        if anUser.userId == -1 {
            noUser = true
        }
        XCTAssertTrue(noUser)
    }
}
