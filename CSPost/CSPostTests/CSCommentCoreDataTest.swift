//
//  CSCoreDataTest.swift
//  CSPostTests
//
//  Created by Pramuka Dias on 3/14/21.
//  Copyright © 2021 Pramuka Dias. All rights reserved.
//

@testable import CSPost
import XCTest

class CSCommentCoreDataTest: XCTestCase {

    let aPost = CSRPost()
    
    override func setUp() {
        super.setUp()
        aPost.postId = 2
        aPost.postTitle = "Dummy Title"
        aPost.postBody = "Dummy Body"
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testGetValidCommentsArrayFromPostId(){
        var isValidCommentArray = false
        let commentsArray = CSCoreDataHandler.getComments(postId: "\(aPost.postId ?? 0)")
        if !commentsArray.isEmpty{
            for aComment in commentsArray{
                if aComment.postId == 2{
                    isValidCommentArray = true
                }else{
                    isValidCommentArray = false
                    return
                }
            }
            XCTAssertTrue(isValidCommentArray)
        }
    }
    
    func testInvalidCommentsArrayFromPostId(){
        var isValidCommentArray = false
        let commentsArray = CSCoreDataHandler.getComments(postId: "\(aPost.postId ?? 0)")
        if !commentsArray.isEmpty{
            for aComment in commentsArray{
                if aComment.postId == 3{
                    isValidCommentArray = true
                }else{
                    isValidCommentArray = false
                    return
                }
            }
            XCTAssertFalse(isValidCommentArray)
        }
    }
    
    func testCommentsArrayEmptyFromPostId(){
        aPost.postId = 24656
        var isEmptyCommentArray = false
        let commentsArray = CSCoreDataHandler.getComments(postId: "\(aPost.postId ?? 0)")
        if !commentsArray.isEmpty{
            isEmptyCommentArray = false
        }else{
            isEmptyCommentArray = true
        }
        XCTAssertTrue(isEmptyCommentArray)
    }
    
    func testCommentsArrayIsNotEmptyFromPostId(){
        aPost.postId = 3
        var isNotEmptyCommentArray = false
        let commentsArray = CSCoreDataHandler.getComments(postId: "\(aPost.postId ?? 0)")
        if !commentsArray.isEmpty{
            isNotEmptyCommentArray = true
        }else{
            isNotEmptyCommentArray = false
        }
        XCTAssertTrue(isNotEmptyCommentArray)
    }
}
